
[build-system]
build-backend = "poetry.core.masonry.api"
requires = ["poetry-core>=1.8.2,<2"]

[project]
name = "ska-sdp-realtime-receive-modules"
version = "6.0.0"
description = "Receive modules used in the realtime workflows. Specifically the visibility receive"
authors = [
    { name = "Stephen Ord", email = "stephen.ord@csiro.au" },
    { name = "Rodrigo Tobar", email = "rtobar@icrar.org" },
    { name = "Callan Gray", email = "callan.gray@icrar.org" },
    { name = "Mark Boulton", email = "mark.boulton@icrar.org" },
    { name = "Julian Carrivick", email = "julian.carrivick@uwa.edu.au" },
]

[[tool.poetry.source]]
name = "PyPI"
priority = "primary"

[[tool.poetry.source]]
name = "skao"
url = "https://artefact.skao.int/repository/pypi-internal/simple"
priority = "supplemental"

[[tool.poetry.source]]
name = "ska-sdp-cbf-emulator"
url = "https://gitlab.com/api/v4/projects/18026017/packages/pypi/simple"
priority = "supplemental"

[tool.poetry]
name = "ska-sdp-realtime-receive-modules"
version = "6.0.0"
description = "Receive modules used in the realtime workflows. Specifically the visibility receive"
authors = [
    "Stephen Ord <stephen.ord@csiro.au>",
    "Rodrigo Tobar <rtobar@icrar.org>",
    "Callan Gray <callan.gray@icrar.org>",
    "Mark Boulton <mark.boulton@icrar.org>",
    "Julian Carrivick <julian.carrivick@uwa.edu.au>",
]
license = "BSD-3-Clause"
packages = [{ include = "realtime", from = "src" }]

[tool.poetry.scripts]
emu-recv = "realtime.receive.modules.receiver:main"
vis-receiver = "realtime.receive.modules.receiver:main"



[tool.poetry.dependencies] # until poetry adopts https://peps.python.org/pep-0631/
python = "^3.10"
ska-sdp-config = { version = "^1.0.0", source = "skao" }
ska-sdp-dal = { version = ">= 0.2.1", source = "skao" }
ska-sdp-dal-schemas = { version = "~0.6.1", source = "skao" }
ska-sdp-datamodels = { version = "^1.0.0", source = "skao" }
ska-sdp-realtime-receive-core = { version = "^7.0.0", source = "skao" }
ska-ser-logging = { version = "^0.4.1", source = "skao" }
ska-telmodel = { version = "^1.8.1", source = "skao" }
aiokafka = ">=0.11.0,<0.13.0"
astropy = "*"
numpy = "^1.23.0"
overrides = "^7.0"
python-casacore = ">= 3.1.1"
spead2 = "^4.1.0"
pyarrow = "== 11.0.0"
pyyaml = "^6.0"

[tool.poetry.group.lint.dependencies]
black = ">=24.4.2"
flake8 = ">=7.1.0"
isort = ">=5.6.4"
pylint = ">=3.2.5"
pylint_junit = ">=0.3.2"
toml-sort = ">=0.20.1"

[tool.poetry.group.test.dependencies]
pytest = "^8.3.4"
pytest-asyncio = "^0.25.2"
pytest-bdd = "^8.1.0"
pytest-benchmark = "^5.1.0"
pytest-cov = "^6.0.0"
ska-sdp-cbf-emulator = { version = "^9.0.0", source = "skao" }

[tool.poetry.group.docs.dependencies]
astropy = "*"
markupsafe = "^2.1"
pygments = ">=2.13"
recommonmark = ">=0.7.1"
ska-sdp-realtime-receive-core = { version = "^7.0.0", source = "skao" }
sphinx = ">=5.3.0"
sphinx-autobuild = ">=2021.3.14"
sphinx_pyproject = ">=0.1.0"
sphinx_rtd_theme = ">=1.1.1"
sphinxcontrib-websupport = ">=1.2.4"

[tool.pylint.MASTER]
ignore = 'version.py'

[tool.pylint.BASIC]
# Don't require docs on private or test methods/classes
no-docstring-rgx = '^_|^test_|^Test'

[tool.pylint.TYPECHECK]
ignored-classes = [
    'optparse.Values',
    'thread._local',
    '_thread._local',
    'astropy.units',
]

[tool.pylint.MISCELLANEOUS]
notes = ['FIXME', 'XXX']

[tool.pylint.'MESSAGES CONTROL']
disable = [
    'too-few-public-methods',
    'too-many-arguments',
    'too-many-locals',
    'too-many-instance-attributes',
    'too-many-positional-arguments',
    'duplicate-code',
    'no-else-return',
    'no-else-raise',
    'no-else-continue',
    'invalid-name',
    'missing-module-docstring',
    # We handle this with isort
    'wrong-import-order',
    'ungrouped-imports',
    # Black sometimes allows lines that are longer (e.g. to avoid breaking string)
    'line-too-long',
]

[tool.isort]
profile = "black"
line_length = 99

[tool.black]
line-length = 99

[tool.pytest.ini_options]
testpaths = "tests"
junit_family = "xunit1"
addopts = [
    "--show-capture=all",
    "--log-level=INFO",
    "--cov=src",
    "--cov-append",
    "-vv",
    "--gherkin-terminal-reporter",
    "--cov-report",
    "term",
    "--cov-report",
    "html",
    "--cov-report",
    "xml",
    "--junitxml=./build/reports/unit-tests.xml",
    "--cucumber-json=./build/cucumber.json",
    "--benchmark-skip",
]
filterwarnings = [
	"ignore:.*XTP-.*",
	"ignore:.*Plasma is deprecated since Arrow 10\\.0\\.0\\.*",
]

[tool.coverage.run]
branch = true
