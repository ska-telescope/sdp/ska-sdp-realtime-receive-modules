import asyncio
from typing import Optional, Union

from ska_sdp_cbf_emulator import packetiser

from realtime.receive.modules.receiver import ReceiverConfig, receive


async def _wait_and_send(
    sender_config: packetiser.SenderConfig,
    recv_ready_event: asyncio.Event,
):
    await recv_ready_event.wait()
    sender = packetiser.packetise(sender_config)
    result = await sender
    return result


async def wait(tasks, timeout):
    """
    Waits and asserts each task is done within the timeout. On success it
    resturns the tasks' results in order, otherwise it raises an AssertionError
    with a message containing: a) the results for the tasks that have finished,
    b) a stack trace for each of the tasks that did not finish, and c) a stack
    trace for all tasks in the asyncio loop.
    """
    done, pending = await asyncio.wait(tasks, timeout=timeout, return_when=asyncio.FIRST_EXCEPTION)
    if pending:
        # pylint: disable=unnecessary-lambda-assignment
        stack2str = lambda stack: "\n    ".join(repr(frame) for frame in stack)
        task2str = lambda task: f"Task {task!r}:\n    {stack2str(task.get_stack())}"
        tasks2str = lambda tasks: "\n".join(task2str(task) for task in tasks)
        for task in pending:
            task.cancel()
        message = (
            f"Done task(s) results:\n{[task.result() for task in done]}\n"
            f"Pending task(s) we waited for:\n{tasks2str(pending)}\n"
            f"All task(s) still pending:\n{tasks2str(asyncio.all_tasks())}"
        )
        raise AssertionError(message)
    return [task.result() for task in tasks]


def create_send_and_receive_tasks(
    sender_config: packetiser.SenderConfig,
    receiver_config: ReceiverConfig,
):
    """
    Schedules the execution of a sender and receiver with the given
    configuration and input file so that the sender starts right after the
    receiver has finished setting itself up, ensuring the receiver streams are
    ready to receive data.
    """

    ready_event = asyncio.Event()
    return [
        asyncio.create_task(coro)
        for coro in (
            _wait_and_send(sender_config, ready_event),
            receive(receiver_config, receiver_ready_evt=ready_event),
        )
    ]


async def send_and_receive(
    sender_config: packetiser.SenderConfig,
    receiver_config: ReceiverConfig,
    timeout: Optional[Union[int, float]] = None,
):
    """
    Like `create_send_and_receive_tasks`, but additionally awaits both for the
    sender and receiver with the given timeout, after which they should have
    finished.
    """
    tasks = create_send_and_receive_tasks(sender_config, receiver_config)
    return await wait(tasks, timeout)
