import asyncio
import os
import tempfile

import pytest
from common import send_and_receive
from pytest_bdd import scenario
from pytest_bdd.steps import given, then, when
from ska_sdp_cbf_emulator import transmitters
from ska_sdp_cbf_emulator.packetiser import MeasurementSetDataSourceConfig, SenderConfig

from realtime.receive.modules import consumers, receivers
from realtime.receive.modules.receiver import ReceiverConfig

# pylint: disable=missing-function-docstring

INPUT_FILE = "data/AA05LOW.ms"
NUM_STREAMS = 96
output_ms = tempfile.mktemp(suffix=".ms", prefix="output_")


@pytest.fixture(name="loop")
def get_loop():
    return asyncio.new_event_loop()


@pytest.mark.xfail(reason="CI has network issues and drops UDP packets")
@scenario(
    "./YAN-716.feature",
    "Emulator sends at AA0.5 rate",
)
def test_aa0_5_rate():
    pass


@given("An example input file of the correct dimension")
def test_file():
    if os.path.isdir(INPUT_FILE):
        return
    else:
        raise FileExistsError


@given("A receiver to capture the data", target_fixture="receiver_config")
def get_receiver_config():
    config = ReceiverConfig()
    config.reception = receivers.create_config(
        port_start=22001,
        num_streams=NUM_STREAMS,
        ring_heaps=128,
    )
    config.consumer = consumers.create_config(name="null_consumer")
    config.tm.measurement_set = INPUT_FILE
    config.scan_provider.measurement_set = INPUT_FILE
    config.uvw_engine.engine = "measures"
    return config


@when(
    "the data is sent at a rate commensurate with AA0.5",
    target_fixture="receiver",
)
def send_data(receiver_config, loop):
    rate = 1e9 / NUM_STREAMS

    sender_config = SenderConfig()
    sender_config.time_interval = 0
    sender_config.transmission = transmitters.create_config(
        target_host="127.0.0.1",
        target_port_start=22001,
        num_streams=NUM_STREAMS,
        rate=rate,
    )
    sender_config.ms = MeasurementSetDataSourceConfig(INPUT_FILE, num_repeats=10, num_timestamps=2)

    # Go, go, go!
    _, receiver = loop.run_until_complete(
        send_and_receive(sender_config, receiver_config, timeout=30)
    )
    return receiver


@then("It is received without loss")
def received_ok(receiver):
    assert receiver.stats.num_incomplete == 0, "Failed to send data without loss"
