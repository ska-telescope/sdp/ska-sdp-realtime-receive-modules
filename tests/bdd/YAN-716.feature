# Created by ord006 at 29/6/21
Feature:

  Scenario: Emulator sends at AA0.5 rate

    Given An example input file of the correct dimension
    And A receiver to capture the data
    When the data is sent at a rate commensurate with AA0.5
    Then It is received without loss


