import asyncio

import pytest

from realtime.receive.modules.utils.periodic_task import PeriodicTask


@pytest.mark.parametrize(
    "period, total_time, valid_count_check",
    (
        (100, 0.5, lambda count: count == 0),
        (0.01, 0.5, lambda count: count > 0),
    ),
)
@pytest.mark.asyncio
async def test_periodic_task(period, total_time, valid_count_check):
    """Check that PeriodicTask executes a coroutine in the background"""
    count = 0

    async def counter():
        nonlocal count
        count += 1

    # Run as many times as possible within the given period
    async with PeriodicTask(period, counter):
        await asyncio.sleep(total_time)
    assert valid_count_check(count)


@pytest.mark.asyncio
async def test_raising_callback_is_invoked_multiple_times():
    """Check that a callback that raises still gets called periodically"""

    condition = asyncio.Condition()

    async def notify_and_raise():
        async with condition:
            condition.notify()
        raise ValueError

    async def wait_for_notifications(n_notifications: int):
        for _ in range(n_notifications):
            async with condition:
                await condition.wait()

    # Run and check we get all notifications, even though the callback raises an error
    period = 0.01
    async with PeriodicTask(period, notify_and_raise):
        done, _waiting = await asyncio.wait(
            [asyncio.create_task(wait_for_notifications(2))], timeout=5
        )
        assert len(done) == 1
