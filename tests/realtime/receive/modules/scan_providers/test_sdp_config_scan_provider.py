import asyncio
import contextlib
from typing import AsyncGenerator

import numpy as np
import pytest
import pytest_asyncio
from common import EmulatedSDP, SubarrayCommand
from overrides import override

from realtime.receive.modules.scan_lifecycle_handler import ScanLifecycleHandler
from realtime.receive.modules.scan_providers.sdp_config_scan_provider import (
    SdpConfigDBUpdateHandler,
    SdpConfigScanProvider,
)


class _CountingScanLifecycleHandler(ScanLifecycleHandler):
    def __init__(self):
        self._scans_started: list[int] = []
        self._scans_ended: list[int] = []
        self._start_scan_event = asyncio.Event()
        self._end_scan_event = asyncio.Event()

    @override
    async def start_scan(self, scan_id: int) -> None:
        self._scans_started.append(scan_id)
        self._start_scan_event.set()

    @override
    async def end_scan(self, scan_id: int) -> None:
        self._scans_ended.append(scan_id)
        self._end_scan_event.set()

    async def _pop_scan_transition(
        self, event: asyncio.Event, scan_ids: list, timeout: float | None
    ) -> int | None:
        try:
            if timeout is None and not event.is_set():
                raise asyncio.TimeoutError
            if timeout is not None:
                await asyncio.wait_for(event.wait(), timeout=timeout)
        except asyncio.TimeoutError:
            assert not scan_ids
            return None
        else:
            assert scan_ids
            return scan_ids.pop(0)
        finally:
            event.clear()

    async def pop_start_scan(self, timeout: float | None = None) -> int | None:
        """Wait until the next start_scan call has occurred, with a timeout"""
        return await self._pop_scan_transition(
            self._start_scan_event, self._scans_started, timeout
        )

    async def pop_end_scan(self, timeout: float | None = None) -> int | None:
        """Wait until the next start_scan call has occurred, with a timeout"""
        return await self._pop_scan_transition(self._end_scan_event, self._scans_ended, timeout)


class _BlockingDBUpdateHandler(SdpConfigDBUpdateHandler):
    def __init__(self):
        self._event = asyncio.Event()
        self._event.clear()

    @override
    async def db_updated(self, eb_state: dict) -> None:
        self._event.set()

    async def wait_for_update(self, timeout) -> None:
        """Wait until we have been notified a DB update, with a timeout"""
        try:
            await asyncio.wait_for(self._event.wait(), timeout=timeout)
            self._event.clear()
        except asyncio.TimeoutError as exc:
            raise AssertionError(f"Update didn't appear in {timeout:.2f} seconds") from exc


class ScanProviderTestHarness:
    """
    Contains an SdpConfigScanProvider equipped with testing utilities
    """

    def __init__(self, emulated_sdp: EmulatedSDP, end_scan_max_delay: float):
        self._emulated_sdp = emulated_sdp
        self.end_scan_max_delay = end_scan_max_delay
        self._counting_handler = _CountingScanLifecycleHandler()
        self._db_update_handler = _BlockingDBUpdateHandler()
        scan_provider = SdpConfigScanProvider(
            emulated_sdp.EB_ID, emulated_sdp.sdp_config, end_scan_max_delay
        )
        scan_provider.add_scan_lifecycle_handler(self._counting_handler)
        scan_provider.add_db_update_handler(self._db_update_handler)
        self.scan_provider = scan_provider

    async def wait_for_db_update(self, timeout=10):
        """Wait until we have been notified a DB update, with a timeout"""
        await self._db_update_handler.wait_for_update(timeout)

    async def pop_start_scan(self, timeout: float | None = None) -> int | None:
        """Wait until the next start_scan transition has occurred, with a timeout"""
        return await self._counting_handler.pop_start_scan(timeout)

    async def pop_end_scan(self, timeout: float | None = None) -> int | None:
        """Wait until the next end_scan transition has occurred, with a timeout"""
        return await self._counting_handler.pop_end_scan(timeout)

    def _assert_scan(self, scan, scan_id):
        assert scan.scan_number == scan_id
        assert scan.scan_type.scan_type_id == self._emulated_sdp.scan_type_id
        np.testing.assert_allclose(
            np.array([[0.709823297846581], [-0.00023193486504280203]]),
            np.array(
                [
                    scan.scan_type.beams[0].field.phase_dir.ra.rad,
                    scan.scan_type.beams[0].field.phase_dir.dec.rad,
                ]
            ),
            rtol=1e-15,
        )

    async def _get_and_assert_scan(self, scan_id):
        for _ in range(100):
            scan = self.scan_provider.get(scan_id)
            if scan:
                break
            await asyncio.sleep(0.1)
        else:
            raise AssertionError("Scan wasn't found in 10 seconds")
        self._assert_scan(scan, scan_id)

    async def assert_effects(
        self,
        command: SubarrayCommand,
        scan_started: int | None = None,
        scan_ended: int | None = None,
        scan_id_does_not_exist: int | None = None,
    ):
        """
        Verifies that after the given command has been sent to the SDP Subarray,
        a number of things occur, namely:
         * At least one DB update is observed by the scan provider
         * The current scan as seen by the scan provider is what's expected
         * The scan lifecycle handlers observe the expected events
         * scan provider's get() method works as expected
        """

        # Note that even though SDP issues two updates per command (at the beginning and at the end),
        # it shouldn't matter which one we react on, as the scan_id in the EB state is the same
        await self.wait_for_db_update(timeout=2)

        # Scan provider status
        if command == SubarrayCommand.SCAN:
            assert self.scan_provider.current_scan
            self._assert_scan(self.scan_provider.current_scan, scan_started)
        else:
            assert self.scan_provider.current_scan is None or self.scan_provider.end_scan_pending

        # Scan transitions as seen by ScanLifecycleHandlers
        for expected_scan_id, scan_id_popper in (
            (scan_started, self.pop_start_scan),
            (scan_ended, self.pop_end_scan),
        ):
            if expected_scan_id is not None:
                assert (await scan_id_popper()) == expected_scan_id
            else:
                assert await scan_id_popper() is None

        # Keeping this around because it was previously tested for, it will
        # disappear once ScanProvider.get() is gone.
        if scan_started is not None:
            await self._get_and_assert_scan(scan_started)
        if scan_id_does_not_exist is not None:
            assert self.scan_provider.get(scan_id_does_not_exist) is None


@contextlib.asynccontextmanager
async def _harness_with_delay(
    emulated_sdp: EmulatedSDP, end_scan_max_delay: float
) -> AsyncGenerator[ScanProviderTestHarness, None]:
    harness = ScanProviderTestHarness(emulated_sdp, end_scan_max_delay=end_scan_max_delay)
    async with harness.scan_provider:
        # Starting the background monitoring thread should trip the DB update handler
        await harness.wait_for_db_update(timeout=10)
        yield harness


@pytest_asyncio.fixture(name="scan_provider_harness_with_delay")
async def _scan_provider_harness_with_delay(
    emulated_sdp: EmulatedSDP, end_scan_max_delay: float
) -> AsyncGenerator[ScanProviderTestHarness, None]:
    async with _harness_with_delay(emulated_sdp, end_scan_max_delay) as harness:
        yield harness


@pytest_asyncio.fixture(name="scan_provider_harness_already_scanning")
async def _scan_provider_harness_already_scanning(
    emulated_sdp: EmulatedSDP, scanning_scan_id: int
) -> AsyncGenerator[ScanProviderTestHarness, None]:
    emulated_sdp.run_scan_command(scanning_scan_id)
    async with _harness_with_delay(emulated_sdp, 0) as harness:
        yield harness


@pytest_asyncio.fixture(name="scan_provider_harness")
async def _scan_provider_harness(emulated_sdp) -> AsyncGenerator[ScanProviderTestHarness, None]:
    async with _harness_with_delay(emulated_sdp, 0) as harness:
        yield harness


@pytest.mark.asyncio
async def test_sdp_config_scan_provider_missing_eb(sdp_config):
    # EB doesn't exist
    invalid_eb_id = "eb-missing-00000000-0000"
    with pytest.raises(ValueError, match=invalid_eb_id):
        SdpConfigScanProvider(invalid_eb_id, sdp_config)


@pytest.mark.asyncio
async def test_pristine_sdp_config_scan_provider(
    scan_provider_harness: ScanProviderTestHarness, emulated_sdp: EmulatedSDP
):
    scan_provider = scan_provider_harness.scan_provider
    # At creation time we should be able to read all scan types
    # pylint: disable-next=protected-access
    assert len(scan_provider._scan_types) == len(emulated_sdp.eblock.scan_types)
    assert scan_provider.current_scan is None
    assert await scan_provider_harness.pop_end_scan() is None


@pytest.mark.parametrize("scanning_scan_id", [1])
@pytest.mark.asyncio
async def test_scan_provider_on_scanning_sdp(
    scan_provider_harness_already_scanning: ScanProviderTestHarness,
    emulated_sdp: EmulatedSDP,
    scanning_scan_id: int,
):
    # the scan provider should pick up that SDP is already scanning and not
    # issue a start_scan event
    harness = scan_provider_harness_already_scanning
    scan_provider = harness.scan_provider
    assert scan_provider.current_scan is not None
    assert await harness.pop_start_scan() is None

    emulated_sdp.run_end_scan_command()
    harness.assert_effects(SubarrayCommand.END_SCAN, scan_ended=scanning_scan_id)


@pytest.mark.parametrize("num_scans", [1, 2, 10])
@pytest.mark.asyncio
async def test_sdp_config_scan_provider_multiple_scans_from_scratch(
    scan_provider_harness: ScanProviderTestHarness, emulated_sdp: EmulatedSDP, num_scans
):
    """Test against a well behaved subarray performing an observation"""

    scan_type_id = emulated_sdp.scan_type_id

    # AssignResources + Configure [[ + Scan + EndScan ]] x num_scans
    emulated_sdp.update_db_state(
        {"scan_type": None, "scan_id": None},
        {"command": SubarrayCommand.ASSIGN_RESOURCES.value},
    )
    await scan_provider_harness.assert_effects(SubarrayCommand.ASSIGN_RESOURCES)
    emulated_sdp.update_db_state(
        {"scan_type": scan_type_id, "scan_id": None},
        {"command": SubarrayCommand.CONFIGURE.value},
    )
    await scan_provider_harness.assert_effects(SubarrayCommand.CONFIGURE)
    for scan_id in range(1, num_scans + 1):
        emulated_sdp.run_scan_command(scan_id)
        await scan_provider_harness.assert_effects(SubarrayCommand.SCAN, scan_started=scan_id)
        emulated_sdp.run_end_scan_command()
        await scan_provider_harness.assert_effects(SubarrayCommand.END_SCAN, scan_ended=scan_id)


@pytest.mark.asyncio
async def test_sdp_config_scan_provider_invalid_eb_content(
    scan_provider_harness: ScanProviderTestHarness, emulated_sdp: EmulatedSDP
):
    """Test against invalid Execution Block contents"""

    scan_type_id = emulated_sdp.scan_type_id
    valid_subarray_update = {"command": SubarrayCommand.SCAN.value}

    invalid_db_updates = (
        # EB missing scan_id
        ({"scan_type": scan_type_id, "scan_id": None}, valid_subarray_update),
        # EB missing current_scan_type
        ({"scan_type": None, "scan_id": 0}, valid_subarray_update),
    )

    for eb_update, subarray_update in invalid_db_updates:
        emulated_sdp.update_db_state(eb_update, subarray_update)
        await scan_provider_harness.assert_effects(
            SubarrayCommand.ASSIGN_RESOURCES, scan_id_does_not_exist=0
        )


@pytest.mark.asyncio
async def test_end_command_missing(
    scan_provider_harness: ScanProviderTestHarness, emulated_sdp: EmulatedSDP
):
    """
    Test two consecutive Scan commands without an End command in between.
    It shouldn't occur, unless commands are issued in quick succession and we
    only get the last SDP Config DB update, but we still handle it. Here we issue
    them explicitly.
    """

    harness = scan_provider_harness

    # The scan provider forcibly ends the current scan and starts a new one.
    scan_id = 1
    emulated_sdp.run_scan_command(scan_id)
    await harness.assert_effects(SubarrayCommand.SCAN, scan_started=scan_id)
    emulated_sdp.run_scan_command(scan_id + 1)
    await harness.assert_effects(
        SubarrayCommand.SCAN, scan_ended=scan_id, scan_started=scan_id + 1
    )


class MockReceiver(ScanLifecycleHandler):
    """A mock receiver that simulates the reception of data until end_scan is received"""

    def __init__(self, sdp_config_scan_provider: SdpConfigScanProvider):
        self._sdp_config_scan_provider = sdp_config_scan_provider
        self._receiving = False

    @property
    def receiving(self) -> bool:
        """Whether data heaps are being received"""
        return self._receiving

    async def reception_started(self, scan_id):
        """Simulate the reception of the first data heap for the given scan ID"""
        self._receiving = True
        await self._sdp_config_scan_provider.first_scan_data_received(scan_id)

    async def reception_ended(self):
        """Simulate the closing of all streams, all data has been received"""
        self._receiving = False
        await self._sdp_config_scan_provider.last_scan_data_received()

    async def start_scan(self, scan_id: int) -> None:
        pass

    async def end_scan(self, scan_id: int) -> None:
        self._receiving = False


SHORT_DELAY = 0.01
LONG_DELAY = 10000


@pytest.mark.parametrize("end_scan_max_delay", [SHORT_DELAY, LONG_DELAY])
@pytest.mark.asyncio
async def test_delayed_end_scan(
    scan_provider_harness_with_delay: ScanProviderTestHarness, emulated_sdp: EmulatedSDP
):
    harness = scan_provider_harness_with_delay
    mock_receiver = MockReceiver(scan_provider_harness_with_delay.scan_provider)
    scan_provider_harness_with_delay.scan_provider.add_scan_lifecycle_handler(mock_receiver)

    # Start the first scan
    first_scan_id = 1
    await mock_receiver.reception_started(first_scan_id)
    assert mock_receiver.receiving
    emulated_sdp.run_scan_command(first_scan_id)
    await harness.assert_effects(SubarrayCommand.SCAN, scan_started=first_scan_id)
    assert mock_receiver.receiving

    # EndScan arrives, but the receiver is still receiving data.
    # EndScan isn't immediately processed, and it doesn't stop the receiver
    emulated_sdp.run_end_scan_command()
    await harness.assert_effects(SubarrayCommand.END_SCAN, scan_ended=None)
    assert mock_receiver.receiving

    # Wait for EndScan to be processed, only if the delay is short;
    # otherwise it will be forcefully ended in the next Scan command
    if harness.end_scan_max_delay == SHORT_DELAY:
        assert await harness.pop_end_scan(timeout=1) == first_scan_id
        assert not mock_receiver.receiving
        second_scan_command_assertions = {}
    else:
        second_scan_command_assertions = {"scan_ended": first_scan_id}

    # A second scan starts, first scan is forced to end if the delay was long,
    # shutting down the receiver
    second_scan_id = first_scan_id + 1
    emulated_sdp.run_scan_command(second_scan_id)
    await harness.assert_effects(
        SubarrayCommand.SCAN, scan_started=second_scan_id, **second_scan_command_assertions
    )
    assert not mock_receiver.receiving

    # The receiver receives all data for the second scan before EndScan,
    # EndScan is processed immediately
    await mock_receiver.reception_started(second_scan_id)
    await mock_receiver.reception_ended()
    emulated_sdp.run_end_scan_command()
    await harness.assert_effects(SubarrayCommand.END_SCAN, scan_ended=second_scan_id)
    assert not mock_receiver.receiving

    # A third scan starts, EndScan arrives and is delayed. In the meantime the receiver finishes
    # receiving from CBF, so EndScan is processed
    third_scan_id = first_scan_id + 2
    emulated_sdp.run_scan_command(third_scan_id)
    await harness.assert_effects(SubarrayCommand.SCAN, scan_started=third_scan_id)
    await mock_receiver.reception_started(third_scan_id)
    emulated_sdp.run_end_scan_command()
    await harness.assert_effects(SubarrayCommand.END_SCAN, scan_ended=None)
    await mock_receiver.reception_ended()
    assert await harness.pop_end_scan(timeout=2) == third_scan_id

    # A fourth scan starts. EndScan arrives and is delayed. The scan provider is stopped,
    # so EndScan is processed
    fourth_scan_id = first_scan_id + 3
    emulated_sdp.run_scan_command(fourth_scan_id)
    await harness.assert_effects(SubarrayCommand.SCAN, scan_started=fourth_scan_id)
    assert not mock_receiver.receiving
    await mock_receiver.reception_started(fourth_scan_id)
    emulated_sdp.run_end_scan_command()
    await harness.assert_effects(SubarrayCommand.END_SCAN, scan_ended=None)
    assert mock_receiver.receiving
    await harness.scan_provider.aclose()
    assert not mock_receiver.receiving
    assert await harness.pop_end_scan(timeout=2) == fourth_scan_id
