import asyncio
import pathlib
import sys
import tempfile

import pytest
import yaml


async def _run_receiver_proc(*extra_args):
    retries = 100
    period = 0.1
    with tempfile.TemporaryDirectory() as tmpdir:
        readiness_file = pathlib.Path(tmpdir) / "ready"
        args = (
            "-m",
            "realtime.receive.modules.receiver",
            "-o",
            f"reception.readiness_filename={readiness_file}",
            "-o",
            "scan_provider.hardcoded_spectral_window_channels=0:1:1",
            "-o",
            "telescope_model.hardcoded_num_receptors=6",
        ) + extra_args
        receiver_proc = await asyncio.create_subprocess_exec(sys.executable, *args)
        try:
            for _ in range(retries):
                if readiness_file.exists():
                    break
                await asyncio.sleep(period)
            else:
                raise AssertionError("receiver didn't start in time")
        finally:
            receiver_proc.terminate()
            assert 0 == await receiver_proc.wait()


@pytest.mark.parametrize("config", ({}, {"reception": {"num_streams": 10}}))
@pytest.mark.asyncio
async def test_cli_with_config_file(config):
    with tempfile.NamedTemporaryFile("wt") as config_file:
        yaml.dump(config, config_file)
        config_file.flush()
        await _run_receiver_proc("-c", config_file.name)


@pytest.mark.asyncio
async def test_cli_without_config_file():
    await _run_receiver_proc()
