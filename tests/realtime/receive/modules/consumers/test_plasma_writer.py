import asyncio
import contextlib
import logging
import subprocess as sp

import numpy as np
import pytest
import sdp_dal_schemas
import ska_sdp_dal
from common import wait
from numpy.testing import assert_array_equal
from realtime.receive.core import ChannelRange, icd, msutils, time_utils
from realtime.receive.core.uvw_engine import UVWEngine
from ska_sdp_dal.connection import Connection, TensorRef

from realtime.receive.modules.consumers import plasma_writer
from realtime.receive.modules.payload_data import PayloadData
from realtime.receive.modules.scan_providers import HardcodedScanProvider
from realtime.receive.modules.tm import MeasurementSetTM
from realtime.receive.modules.utils import sdp_visibility

INPUT_FILE = "data/sim-vis.ms"
PLASMA_SOCKET = "/tmp/plasma"


logger = logging.getLogger(__name__)


@pytest.fixture(name="plasma_store")
def _plasma_store():
    with sp.Popen(["plasma_store", "-m", "100000000", "-s", PLASMA_SOCKET]) as store:
        yield store
        store.terminate()


def _assert_consumer_is_in_valid_state(consumer):
    # pylint: disable=protected-access
    assert all(len(refs) == 2 for refs in consumer._rpc_inout_refs)
    assert len(consumer._rpc_inout_refs) <= consumer.config.payloads_in_flight


def _create_consumer(**config_kwargs):
    tm = MeasurementSetTM(INPUT_FILE)
    uvw_engine = UVWEngine(tm.get_antennas())
    config = plasma_writer.PlasmaWriterConfig(plasma_path=PLASMA_SOCKET, **config_kwargs)
    consumer = plasma_writer.PlasmaWriterConsumer(config, tm, uvw_engine)
    _assert_consumer_is_in_valid_state(consumer)
    return consumer


@contextlib.asynccontextmanager
async def _scanning_context(consumer, scan_id):
    await consumer.start_scan(scan_id)
    yield
    await consumer.end_scan(scan_id)


async def _consume_visibility(consumer, visibility):
    _assert_consumer_is_in_valid_state(consumer)
    await consumer.consume(visibility)
    _assert_consumer_is_in_valid_state(consumer)


async def _consume_ms(
    consumer,
    scan_id,
    num_timestamps=1,
    aggregate_payloads=False,
    sw_channels: ChannelRange | None = None,
    payload_channels: ChannelRange | None = None,
):
    _assert_consumer_is_in_valid_state(consumer)
    payload = icd.Payload()
    tm = MeasurementSetTM(INPUT_FILE)

    if not sw_channels:
        with msutils.MeasurementSet.open(INPUT_FILE) as ms:
            sw_channels = ms.channel_range

    scan_provider = HardcodedScanProvider.from_spectral_window(sw_channels)

    if not payload_channels:
        payload_channels = sw_channels

    payloads = []
    sequence_number = 0
    async for vis, timestamp in msutils.vis_reader(INPUT_FILE, num_timestamps=num_timestamps):
        n_baselines, n_freq = vis.shape[0:2]
        assert (
            payload_channels.count <= n_freq
        ), f"Unable to consume {repr(payload_channels)}, only {n_freq} in the MS"

        payload.timestamp = time_utils.mjd_to_unix(timestamp)
        payload.channel_id = payload_channels.start_id
        payload.channel_count = payload_channels.count

        # We don't use channels.as_slice() as there are only 4 channels in
        # the MS which limits how effectively we can test. So just take
        # the right number of channels. Any actual data we verify is the
        # hard-coded overridden data below.
        vis = vis[:, : payload_channels.count]
        payload.visibilities = icd.ms_to_icd(vis)
        payload.visibilities[0][0][2] = complex(1, 2)
        payload.correlated_data_fraction = np.ones(
            [payload_channels.count, n_baselines], dtype=np.uint8
        )
        payloads.append(
            PayloadData.from_payload(payload, sequence_number, scan_provider.get(scan_id))
        )
        sequence_number += 1

    if aggregate_payloads:
        visibility = sdp_visibility.create_visibility(payloads, tm)
        await _consume_visibility(consumer, visibility)
    else:
        for payload in payloads:
            visibility = sdp_visibility.create_visibility([payload], tm)
            await _consume_visibility(consumer, visibility)


@pytest.mark.asyncio
@pytest.mark.parametrize("concurrent_calls", (1, 100))
async def test_consume(plasma_store, concurrent_calls):
    """
    Tests the plasma_writer consumer by having it consume three payloads,
    which triggers writing into plasma and reading the results back.
    The consumer is configured with a single payload in flight, triggering
    the output reading code.
    """
    assert plasma_store  # so it doesn't get unused
    consumer = _create_consumer(payloads_in_flight=1)
    scan_id = 1
    async with _scanning_context(consumer, scan_id):
        consumption_tasks = [
            asyncio.create_task(_consume_ms(consumer, scan_id, num_timestamps=3))
            for _ in range(concurrent_calls)
        ]
        await wait(consumption_tasks, timeout=10)

    # Take the OID of the last vis tensor written to plasma
    # (which was issued in the penultimate call, the last one was end_scan)
    # pylint: disable-next=protected-access
    last_call_inout_refs = consumer._rpc_inout_refs[-2]
    input_refs = last_call_inout_refs[0]
    vis_tensor_ref = input_refs[-1]
    vis_tensor_oid = vis_tensor_ref.oid
    # make sure we can read it from plasma
    plasma_connection = Connection(PLASMA_SOCKET)
    assert plasma_connection.object_exists(vis_tensor_oid)
    # and check it has the magic value we set above
    vis_tensor_ref = TensorRef(plasma_connection, vis_tensor_oid)
    vis = vis_tensor_ref.get()
    assert np.all(vis[0][0][0][2] == [1, 2])
    plasma_connection.client.disconnect()

    await consumer.astop()
    _assert_consumer_is_in_valid_state(consumer)

    # Connection should have finished and references should be clear
    # pylint: disable=protected-access
    assert not consumer._rpc_inout_refs
    assert not consumer._common_refs
    assert not consumer._scan_type_refs


@pytest.mark.asyncio
@pytest.mark.parametrize(
    # Note stride doesn't get sent with the payload (determined from
    # spectral window), so no need to set it and match it to the
    # spectral window in each of these test cases
    "sw_channels, payload_channels, expected_channels",
    [
        (ChannelRange(100, 4), ChannelRange(99, 2), None),
        (ChannelRange(100, 4), ChannelRange(105, 2), None),
        (ChannelRange(100, 4), ChannelRange(101, 4), None),
        (ChannelRange(100, 4), ChannelRange(102, 2), ChannelRange(102, 2, 1)),
        (
            ChannelRange(100, 4, 2),
            ChannelRange(102, 2),
            ChannelRange(102, 2, 2),
        ),
    ],
)
async def test_correct_channel_ids_are_written_to_plasma(
    plasma_store,
    sw_channels: ChannelRange,
    payload_channels: ChannelRange,
    expected_channels: ChannelRange | None,
):
    assert plasma_store

    consumer = _create_consumer(wait_for_all_responses_timeout=0.5)

    async def _do_consume():
        scan_id = 1
        async with _scanning_context(consumer, scan_id):
            await _consume_ms(
                consumer,
                scan_id,
                num_timestamps=1,
                sw_channels=sw_channels,
                payload_channels=payload_channels,
            )

    if expected_channels is None:
        with pytest.raises(ValueError):
            await _do_consume()
        return

    async with MockProcessor() as processor, consumer:
        await _do_consume()

    assert len(processor.call_inputs) == 1
    channel_ids = processor.call_inputs[0]["channel_ids"]
    assert_array_equal(expected_channels.as_np_range(), channel_ids)


class MockProcessor(ska_sdp_dal.Processor):
    """
    A mock processor that reads all inputs
    """

    def __init__(self, respond_to_rpc_call: bool = True):
        self._run_task: asyncio.Task | None = None
        self._respond_to_rpc_call: bool = respond_to_rpc_call
        self.call_inputs = []
        self.scan_events = []
        super().__init__(
            [
                sdp_dal_schemas.PROCEDURES[name]
                for name in ("process_visibility", "start_scan", "end_scan")
            ],
            PLASMA_SOCKET,
        )

    def start_scan(self, scan_id, output):
        """Dummy start_scan implementation"""
        self.scan_events.append(("start", scan_id.get()[0]))
        output.put(np.array([0], dtype="i1"))

    def end_scan(self, scan_id, output):
        """Dummy end_scan implementation"""
        self.scan_events.append(("end", scan_id.get()[0]))
        output.put(np.array([0], dtype="i1"))

    def process_visibility(self, **kwargs):
        """Dummy process_visibility implementation"""
        output = kwargs["output"]
        status = 0
        try:
            # read all inputs to trigger schema validation
            inputs = {}
            for name, arg in kwargs.items():
                if name == "output":
                    continue
                inputs[name] = arg.get()
            self.call_inputs.append(inputs)
        except Exception:  # pylint: disable=broad-exception-caught
            logger.exception("error while reading inputs")
            status = 1
        finally:
            if self._respond_to_rpc_call:
                logger.info("Responding with %d", status)
                output.put(np.array([status], dtype="i1"))

    async def _run(self):
        while True:
            try:
                await asyncio.get_running_loop().run_in_executor(None, self.process, 0.1)
            except asyncio.CancelledError:
                break
        self._conn.client.disconnect()

    async def __aenter__(self):
        self._run_task = asyncio.create_task(self._run())
        return self

    async def __aexit__(self, _exc, _typ, _tb):
        assert self._run_task
        self._run_task.cancel()
        await self._run_task


@pytest.mark.asyncio
@pytest.mark.parametrize("aggregate_payloads", (True, False))
async def test_processor_gets_valid_inputs(plasma_store, aggregate_payloads):
    """
    Checks that the data we write into the Plasma store can be read by a simple
    processor.
    """
    assert plasma_store
    num_payloads = 3
    num_visibilities = 1 if aggregate_payloads else num_payloads
    consumer = _create_consumer(
        wait_for_all_responses_timeout=2,
    )
    scan_id = 1
    processor = MockProcessor()
    async with processor, consumer, _scanning_context(consumer, scan_id):
        await _consume_ms(
            consumer,
            scan_id,
            num_timestamps=num_payloads,
            aggregate_payloads=aggregate_payloads,
        )
    assert consumer.invocation_results == plasma_writer.InvocationResults(
        success=num_visibilities + 2, fail=0, unknown=0
    )


@pytest.mark.asyncio
async def test_payload_in_flight_timeout(plasma_store):
    """
    Checks that a timeout raised when clearing payloads in flight doesn't
    result in an invalid consumer state.
    """
    assert plasma_store
    consumer = _create_consumer(
        payloads_in_flight=1,
        remove_old_references_timeout=0.5,
        wait_for_all_responses_timeout=0.5,
    )
    scan_id = 1
    processor = MockProcessor(respond_to_rpc_call=False)
    async with processor, consumer, _scanning_context(consumer, scan_id):
        await _consume_ms(consumer, scan_id, num_timestamps=3)
        _assert_consumer_is_in_valid_state(consumer)
    _assert_consumer_is_in_valid_state(consumer)


@pytest.mark.asyncio
async def test_consumer_interrupted(plasma_store):
    """
    Checks that the consumer can be stopped correctly even if it's interrupted
    during normal operations.
    """
    assert plasma_store
    consumer = _create_consumer(
        payloads_in_flight=1,
        remove_old_references_timeout=2,
        wait_for_all_responses_timeout=0.5,
    )
    scan_id = 1
    processor = MockProcessor(respond_to_rpc_call=False)
    async with processor, consumer, _scanning_context(consumer, scan_id):
        # Fill queue of payloads in flight
        await _consume_ms(consumer, scan_id, num_timestamps=2)
        # Trigger the removal of the first payload in flight and cancel
        # before it times out
        consume_task = asyncio.create_task(consumer.consume(None))
        await asyncio.sleep(1)
        consume_task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await consume_task
        _assert_consumer_is_in_valid_state(consumer)
    _assert_consumer_is_in_valid_state(consumer)


@pytest.mark.asyncio
async def test_scanning_context(plasma_store):
    assert plasma_store
    consumer = _create_consumer()
    processor = MockProcessor()
    async with processor, consumer:
        # No start_scan issued, which should always occur
        scan_id = 1
        with pytest.raises(AssertionError, match="Unknown scan id"):
            await _consume_ms(consumer, scan_id)

        # Normal situation: invoke both procedures, wait for their responses
        scan_id = 2
        async with _scanning_context(consumer, scan_id):
            pass
        assert consumer.invocations_in_flight == 2
        await consumer.wait_for_all_responses()
        assert processor.scan_events == [("start", scan_id), ("end", scan_id)]
        processor.scan_events.clear()

        # Only end_scan, logs a warning but is allowed
        scan_id = 3
        await consumer.end_scan(scan_id)
        assert consumer.invocations_in_flight == 1
        await consumer.wait_for_all_responses()
        assert processor.scan_events == [("end", scan_id)]
