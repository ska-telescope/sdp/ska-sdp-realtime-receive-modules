import unittest
from dataclasses import dataclass

from realtime.receive.core.common import autocast_fields

from realtime.receive.modules import consumers
from realtime.receive.modules.consumers import Config
from realtime.receive.modules.consumers.consumer import Consumer

# pylint: disable=missing-class-docstring

# The consumer we'll try to load, sets _flag to True when loaded
_flag = False


NAME = __name__ + ".MyConsumer"


@dataclass
@autocast_fields
class MyConsumerConfig(Config):
    name: str = NAME
    a: int = 0


class MyConsumer(Consumer):
    config_class = MyConsumerConfig

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # pylint: disable-next=global-statement
        global _flag
        _flag = True

    async def consume(self, *args, **kwargs):
        pass

    async def astop(self):
        pass


class TestUserProvidedConsumer(unittest.TestCase):
    def test_can_load_user_provided_consumer(self):
        """Test we can load user-provided consumer classes"""
        config = consumers.create_config(name=NAME, a="1")
        assert isinstance(config, MyConsumerConfig)
        assert config.name == NAME
        assert config.a == 1
        consumers.create(config, object())
        self.assertTrue(_flag)
