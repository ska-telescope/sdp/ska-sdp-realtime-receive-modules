#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the spead2_receive module."""
import asyncio
import logging
import os
import tempfile
import warnings

import pytest
import spead2.send.asyncio
from common import EmulatedSDP, create_send_and_receive_tasks, send_and_receive, wait
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.icd import Telescope
from realtime.receive.core.ms_asserter import MSAsserter
from ska_sdp_cbf_emulator import transmitters
from ska_sdp_cbf_emulator.packetiser import (
    HardcodedDataSourceConfig,
    MeasurementSetDataSourceConfig,
    SenderConfig,
)

from realtime.receive.modules import consumers, receivers
from realtime.receive.modules.consumers import consumer
from realtime.receive.modules.receiver import ReceiverConfig, TelescopeManagerConfig, receive

# test file is 4 antennas, 4 channels
# 0.9 second dumps
# rate should be ~1422 B/s for data
# plus 48 B for the header
# == 1470 B/s
INPUT_FILE = "data/sim-vis.ms"
NUM_DATA_HEAPS = 133
SCHED_BLOCK = "data/assign_resources_1_0.json"
LAYOUT_FILE = "data/low-layout.json"


MS_TM_CONFIG = TelescopeManagerConfig(measurement_set=INPUT_FILE)
SB_TM_CONFIG = TelescopeManagerConfig(schedblock=SCHED_BLOCK, antenna_layout=LAYOUT_FILE)


EXCEPTION_RAISING_CONSUMER = __name__ + ".ExceptionRaisingConsumer"
EVENT_SETTER_CONSUMER = __name__ + ".EventSetterConsumer"


class ExceptionRaisingConsumer(consumer.Consumer):
    """A consumer that raises a very special exception."""

    class VerySpecialException(Exception):
        """A very, very special exception"""

    async def consume(self, *_args, **_kwargs):
        raise ExceptionRaisingConsumer.VerySpecialException()


class EventSetterConsumer(consumer.Consumer):
    """A consumer that sets a global event when it gets any visibility"""

    evt = asyncio.Event()

    async def consume(self, *_args, **_kwargs):
        EventSetterConsumer.evt.set()

    @classmethod
    async def wait_until_visibility_is_consumed(cls, timeout: float):
        """What is says on the tin"""
        await asyncio.wait_for(cls.evt.wait(), timeout)


def _streams_stopped_by_end_scan(caplog: pytest.LogCaptureFixture) -> bool:
    return (
        sum("streams stopped early due to SDP's EndScan" in rec.message for rec in caplog.records)
        > 0
    )


def _get_configs(
    num_streams=1,
    reader_repeats=1,
    transport_protocol="udp",
    delay_start_of_stream_heaps=0,
    rate=147000,
    max_pending_data_heaps=10,
    telescope: Telescope = Telescope.MID,
):
    receiver_config = ReceiverConfig()
    receiver_config.reception = receivers.create_config(
        port_start=21001,
        num_streams=num_streams,
        ring_heaps=NUM_DATA_HEAPS + 2,
        transport_protocol=transport_protocol,
        max_pending_data_heaps=max_pending_data_heaps,
    )
    receiver_config.scan_provider.measurement_set = INPUT_FILE
    receiver_config.tm.measurement_set = INPUT_FILE
    receiver_config.uvw_engine.engine = "measures"
    receiver_config.aggregation.time_period = 0

    sender_config = SenderConfig()
    sender_config.time_interval = -1
    sender_config.transmission = transmitters.create_config(
        target_host="127.0.0.1",
        target_port_start=21001,
        num_streams=num_streams,
        rate=rate,
        transport_protocol=transport_protocol,
        delay_start_of_stream_heaps=delay_start_of_stream_heaps,
        telescope=telescope,
    )
    sender_config.ms = MeasurementSetDataSourceConfig(INPUT_FILE, num_repeats=reader_repeats)
    return receiver_config, sender_config


@pytest.mark.asyncio
class TestSpead2Receiver:
    @pytest.mark.parametrize("telescope", (Telescope.LOW, Telescope.MID))
    @pytest.mark.parametrize(
        "num_streams, reader_repeats, tm_config, drop_heaps, config_items",
        (
            (1, 1, MS_TM_CONFIG, False, {}),
            (1, 1, SB_TM_CONFIG, False, {}),
            (1, 2, MS_TM_CONFIG, False, {}),
            (2, 1, MS_TM_CONFIG, False, {}),
            (2, 2, MS_TM_CONFIG, False, {}),
            (4, 1, MS_TM_CONFIG, False, {}),
            # When consumers receive one payload at a time, this worked because the
            # consumer we used in this test disregarded payloads for channel_id==0,
            # and wrote the rest to the MS. Now that consumers receive a full
            # Visibility, this behavior cannot be achieved the way it used to; instead
            # the consumer would have to set the flags in all visibilities for channel
            # ID 0, then pass the full Visibility to the mswriter consumer, which would
            # also have to write the flags to the MS (it doesn't at the moment).
            # Moreover, we're planning to remove the mswriter consumer in the first
            # place.
            # All in all, this test is becoming less useful, showing that more than
            # testing the receiver itself, it really tested the mswriter consumer.
            # (4, 1, MS_TM_CONFIG, True, {}),
            (4, 1, MS_TM_CONFIG, False, {"transport_protocol": "tcp"}),
        ),
    )
    async def test_send_recv_stream(
        self,
        num_streams: int,
        reader_repeats: int,
        tm_config: tuple,
        drop_heaps: bool,
        config_items: dict,
        telescope: Telescope,
        caplog: pytest.LogCaptureFixture,
    ):
        """Test reception of SPEAD data in various configurations"""
        output_file = tempfile.mktemp(".ms", "output")
        readiness_file = tempfile.mktemp(prefix="receive_ready-")

        receiver_config, sender_config = _get_configs(
            num_streams,
            reader_repeats=reader_repeats,
            telescope=telescope,
            **config_items,
        )
        receiver_config.tm = tm_config
        receiver_config.reception.readiness_filename = readiness_file
        receiver_config.consumer = consumers.create_config(
            name="mswriter", output_filename=output_file
        )

        # receiver will process n streams until an
        # end of stream is read for each.
        with caplog.at_level(logging.DEBUG):
            _, receiver = await send_and_receive(sender_config, receiver_config, timeout=30)
        assert not _streams_stopped_by_end_scan(caplog)

        ms_asserter = MSAsserter()
        if drop_heaps:
            ms_asserter.assert_heaps_dropped(INPUT_FILE, output_file)
        else:
            assert receiver.aggregated_payloads == NUM_DATA_HEAPS * reader_repeats * num_streams
            ms_asserter.assert_ms_data_equal(INPUT_FILE, output_file, num_repeats=reader_repeats)
        assert os.path.exists(readiness_file)

    @pytest.mark.parametrize(
        ("delay_start_of_stream_heaps, max_pending_data_heaps"),
        (
            (1, 10),
            (NUM_DATA_HEAPS - 1, NUM_DATA_HEAPS - 1),
            (NUM_DATA_HEAPS, NUM_DATA_HEAPS),
        ),
    )
    async def test_delayed_data_heap_reception(
        self, delay_start_of_stream_heaps, max_pending_data_heaps, caplog
    ):
        config = {
            "transport_protocol": "tcp",
            "rate": 1e9,
            "delay_start_of_stream_heaps": delay_start_of_stream_heaps,
            "max_pending_data_heaps": max_pending_data_heaps,
        }
        await self.test_send_recv_stream(1, 1, MS_TM_CONFIG, False, config, Telescope.MID, caplog)

    @pytest.mark.parametrize(
        ("delay_start_of_stream_heaps, max_pending_data_heaps, expected_aggregated_payloads"),
        (
            (8, 5, NUM_DATA_HEAPS - (8 - 5)),
            (-1, NUM_DATA_HEAPS, 0),
        ),
    )
    async def test_data_heaps_lost(
        self,
        delay_start_of_stream_heaps,
        max_pending_data_heaps,
        expected_aggregated_payloads,
    ):
        # queue in receiver is not big enough so some data heaps are lost
        receiver_config, sender_config = _get_configs(
            transport_protocol="tcp",
            rate=1e9,
            delay_start_of_stream_heaps=delay_start_of_stream_heaps,
            max_pending_data_heaps=max_pending_data_heaps,
        )
        receiver_config.tm = MS_TM_CONFIG
        receiver_config.consumer.name = "null_consumer"
        _, receiver = await send_and_receive(sender_config, receiver_config, timeout=30)
        assert receiver.aggregated_payloads == expected_aggregated_payloads

    async def _wait_and_send_empty_sos_heap(self, recv_ready_event):
        await recv_ready_event.wait()
        thread_pool = spead2.ThreadPool()
        stream_config = spead2.send.StreamConfig(
            max_packet_size=1472,
            rate=1024 * 1024 * 1024,
            burst_size=10,
            max_heaps=1,
        )
        stream = await spead2.send.asyncio.TcpStream.connect(
            thread_pool=thread_pool,
            config=stream_config,
            endpoints=[("127.0.0.1", 21001)],
        )

        item_group = spead2.send.ItemGroup(flavour=spead2.Flavour(4, 64, 48, 0))
        await stream.async_send_heap(item_group.get_start())
        await stream.async_send_heap(item_group.get_heap())
        await stream.async_send_heap(item_group.get_end())

    async def test_missing_items(self):
        # An improper sender that sets up a heap with missing Item IDs
        receiver_config, _ = _get_configs(transport_protocol="tcp")
        receiver_ready_evt = asyncio.Event()
        tasks = [
            asyncio.create_task(coro)
            for coro in (
                self._wait_and_send_empty_sos_heap(receiver_ready_evt),
                receive(receiver_config, receiver_ready_evt=receiver_ready_evt),
            )
        ]
        _, receiver = await wait(tasks, timeout=5)
        assert receiver.aggregated_payloads == 0


@pytest.mark.parametrize(
    "sender_streams, receiver_streams, channels_per_payload",
    (
        (1, 2, 1),
        (1, 3, 1),
        (1, 4, 1),
        (2, 3, 1),
        (2, 4, 1),
        (3, 4, 1),
        (1, 2, 2),
    ),
)
@pytest.mark.asyncio
async def test_receiver_with_unused_streams(
    sender_streams, receiver_streams, channels_per_payload
):
    """
    Checks that a receiver can open more streams that it receives data onto
    and is still functional.
    """
    NUM_STATIONS = 6
    NUM_CHANNELS = 4
    NUM_TIMESTEPS = 120

    # The aggregator extrapolates the total number of channels from the number
    # of streams * the number of channels in the first payload, so opening
    # streams for a channel count that goes outside of this limit would cause
    # an error
    assert receiver_streams > sender_streams
    assert receiver_streams * channels_per_payload <= NUM_CHANNELS

    receiver_config = ReceiverConfig()
    receiver_config.reception = receivers.create_config(num_streams=receiver_streams)
    receiver_config.scan_provider.hardcoded_spectral_window_channels = ChannelRange(
        2, NUM_CHANNELS, 2
    )
    receiver_config.tm.hardcoded_num_receptors = NUM_STATIONS
    receiver_config.consumer.name = "null_consumer"

    sender_config = SenderConfig()
    sender_config.time_interval = -1
    sender_config.transmission = transmitters.create_config(num_streams=sender_streams, rate=0)
    sender_config.hardcoded = HardcodedDataSourceConfig(
        num_stations=NUM_STATIONS,
        # Use channel_per_payload here instead of NUM_CHANNELS for the same
        # reason as explains above the previous asserts.
        channels=ChannelRange(2, sender_streams * channels_per_payload, 2),
        num_timesteps=NUM_TIMESTEPS,
    )

    sender_task, receiver_task = create_send_and_receive_tasks(sender_config, receiver_config)
    await wait([sender_task], timeout=10)
    receiver_task.cancel()
    receiver = await receiver_task
    assert receiver.aggregated_payloads == NUM_TIMESTEPS * sender_streams


def test_channel_options_deprecated():
    # No options given still means num_channels = 1, so still raises a warning
    for kwargs in ({"num_channels": 10}, {"channels_per_stream": 1}, {}):
        with pytest.warns(DeprecationWarning):
            receivers.create_config(**kwargs)
    with warnings.catch_warnings():
        warnings.simplefilter("error")
        receivers.create_config(num_streams=10)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "pcap_file,port_start,channel_id_start,expected_channel_count,num_receptors,expected_baseline_count,scan_id",
    (
        ("data/mid-single-stream.pcapng", 10000, 74400, 20, 1, 1, 0x143D568D6CE2),
        ("data/low-single-stream.pcap", 20000, 14400, 1, 6, 21, 0x04D3),
    ),
)
async def test_pcap_file(
    pcap_file,
    port_start,
    channel_id_start,
    expected_channel_count,
    num_receptors,
    expected_baseline_count,
    scan_id,
):
    config = ReceiverConfig()
    config.tm = TelescopeManagerConfig(hardcoded_num_receptors=num_receptors)
    config.scan_provider.hardcoded_spectral_window_channels = ChannelRange(
        channel_id_start, expected_channel_count
    )
    config.consumer.name = "accumulating_consumer"
    config.reception = receivers.create_config(
        pcap_file=pcap_file,
        num_streams=1,
        port_start=port_start,
    )
    receiver = await receive(config)
    assert receiver.aggregated_payloads > 0
    assert receiver.consumer.scans_started == [scan_id]
    assert receiver.consumer.scans_ended == [scan_id]
    visibilities = receiver.consumer.visibilities
    assert visibilities
    first_visibility = visibilities[0]
    assert first_visibility.dims["frequency"] == expected_channel_count
    assert first_visibility.dims["baselines"] == expected_baseline_count


@pytest.mark.asyncio
async def test_can_handle_consumer_errors(caplog):
    """Check that consumer errors don't produce full receiver shutdown"""
    receiver_config, sender_config = _get_configs(
        num_streams=1, transport_protocol="tcp", rate=1e9
    )
    receiver_config.consumer.name = EXCEPTION_RAISING_CONSUMER
    with caplog.at_level(logging.ERROR):
        _, receiver = await send_and_receive(sender_config, receiver_config, timeout=30)
    assert receiver.visibilities_generated > 0
    assert receiver.visibilities_consumed == 0
    assert receiver.aggregated_payloads == NUM_DATA_HEAPS
    assert receiver.stats.num_heaps == NUM_DATA_HEAPS + 2
    assert 1 == sum("VerySpecialException" in rec.exc_text for rec in caplog.records)


@pytest.mark.asyncio
async def test_streams_stop_on_end_scan(
    emulated_sdp: EmulatedSDP, caplog: pytest.LogCaptureFixture
):
    NUM_STATIONS = 4
    SCAN_ID = 1
    sdp_sw = emulated_sdp.eblock.channels[0]["spectral_windows"][0]

    # The receiver aggregates data quickly, and uses a consumer that sets a global event
    # on which can wait from this test. This allows us to ensure data has been received
    # before issuing EndScan
    receiver_config, sender_config = _get_configs(num_streams=4, transport_protocol="tcp", rate=0)
    receiver_config.consumer = consumers.create_config(name=EVENT_SETTER_CONSUMER)
    receiver_config.scan_provider.execution_block_id = emulated_sdp.EB_ID
    receiver_config.scan_provider.end_scan_max_delay = 0
    receiver_config.aggregation.time_period = 0.1

    # Sender streams loads of data, and fast, so the receiver receives some but not all of it,
    # in turn provoking it to be closed by EndScan rather than by the CBF streams finishing
    sender_config.scan_id = SCAN_ID
    sender_config.ms = None
    sender_config.hardcoded = HardcodedDataSourceConfig(
        num_stations=NUM_STATIONS,
        channels=ChannelRange(sdp_sw["start"], sdp_sw["count"], sdp_sw["stride"]),
        num_timesteps=100000000,
    )
    sender_config.time_interval = -1

    with caplog.at_level(logging.DEBUG):
        sender_task, receiver_task = create_send_and_receive_tasks(sender_config, receiver_config)
        emulated_sdp.run_scan_command(SCAN_ID)
        await EventSetterConsumer.wait_until_visibility_is_consumed(2)
        emulated_sdp.run_end_scan_command()
        await wait([receiver_task], timeout=10)
        sender_task.cancel()
        with pytest.raises(asyncio.CancelledError):
            await sender_task
    assert _streams_stopped_by_end_scan(caplog)
