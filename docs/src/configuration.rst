Configuration options
=====================

When using the programmatic interface of this package,
the configuration options used by the different parts of the sending operation
are given as objects.
For details see:

 * |receiver-config|, |scan-provider-config|, |tm-config|,
   |uvw-config| and |sdp-config-db-config|
   for the top-level configuration.
 * |aggregation-config|
   for the aggregation configuration.
 * |reception-config| and |spead2-config|
   for the receiver configuration.
 * |consumer-config|, |mswriter-config| and |plasma-config|
   for the consumer configuration.


From nested dictionaries
------------------------

When using the command-line interface,
options can be specified either in a YAML configuration file,
or individually via the :option:`vis-receiver -o` option.
In both cases options live within a group or category,
and have a name and a value,
and thus the overall configuration can be specified
as a top-level dictionary
with group names as keys,
and group dictionaries as values.
To preserve backwards compatibility,
the programmatic entry point
still accepts this dictionary-based configuration approach too.

These options are used to populate
a |receiver-config| object
as follows:

* Within each group,
  the key names are directly mapped
  to the corresponding configuration object member.
* The ``telescope_manager`` group is used to populate
  the options in the |tm-config| object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.tm`.
* The ``scan_provider`` group is used to populate
  the options in the |scan-provider-config| object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.scan_provider`.
* The ``uvw`` group is used to populate
  the options in the |uvw-config| object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.uvw_engine`.
* The ``sdp_config_db`` group is used to populate
  the options in the |sdp-config-db-config| object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.sdp_config_db`.
* The ``aggregation`` group is used to populate
  the options in the |aggregation-config| object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.aggregation`.
* The ``reception`` group is used to populate
  the options in the |reception-config| (or subclass) object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.reception`.
* The ``consumer`` group is used to populate
  the options in the |consumer-config| (or subclass) object
  under :attr:`realtime.receive.modules.receiver.ReceiverConfig.consumer`.

Apart from these rules, the following backwards-compatible names have been kept:

  * ``reception.command_template`` -> ``consumer.command_template``.`
  * ``reception.consumer`` -> ``consumer.name``.
  * ``reception.datamodel`` -> ``scan_provider.measurement_set``.
  * ``reception.datamodel`` -> ``telescope_model.measurement_set``.
  * ``reception.disable_astropy_iers_autodownload`` -> ``uvw.disable_astropy_iers_autodownload``.
  * ``reception.execution_block_id`` -> ``scan_provider.execution_block_id``.
  * ``reception.execution_block_id`` -> ``telescope_model.execution_block_id``.
  * ``reception.layout`` -> ``telescope_model.antenna_layout``.
  * ``reception.max_payloads`` -> ``consumer.max_payloads_per_ms``.
  * ``reception.outputfilename`` -> ``consumer.output_filename``.
  * ``reception.payloads_in_flight`` -> ``consumer.payloads_in_flight``.
  * ``reception.plasma_path`` -> ``consumer.plasma_path``.
  * ``reception.receiver_port_start`` -> ``reception.port_start``.
  * ``reception.sched_block`` -> ``scan_provider.assign_resources_command``.
  * ``reception.schedblock`` -> ``telescope_model.schedblock``.
  * ``reception.sdp_config_backend`` -> ``sdp_config_db.backend``.
  * ``reception.sdp_config_host`` -> ``sdp_config_db.host``.
  * ``reception.sdp_config_port`` -> ``sdp_config_db.port``.
  * ``reception.timestamp_output`` -> ``consumer.timestamp_output``.`
  * ``reception.transport_proto`` -> ``reception.transport_protocol``.
  * ``reception.uvw_engine`` -> ``uvw.engine``.



.. |receiver-config| replace:: :class:`ReceiverConfig <realtime.receive.modules.receiver.ReceiverConfig>`
.. |scan-provider-config| replace:: :class:`ScanProviderConfig <realtime.receive.modules.receiver.ScanProviderConfig>`
.. |tm-config| replace:: :class:`TelescopeManagerConfig <realtime.receive.modules.receiver.TelescopeManagerConfig>`
.. |uvw-config| replace:: :class:`UVWEngineConfig <realtime.receive.modules.receiver.UVWEngineConfig>`
.. |sdp-config-db-config| replace:: :class:`SdpConfigDbConfig <realtime.receive.modules.receiver.SdpConfigDbConfig>`
.. |reception-config| replace:: :class:`receivers.Config <realtime.receive.modules.receivers.Config>`
.. |aggregation-config| replace:: :class:`aggregation.AggregationConfig <realtime.receive.modules.aggregation.AggregationConfig>`
.. |consumer-config| replace:: :class:`consumers.Config <realtime.receive.modules.consumers.Config>`
.. |mswriter-config| replace:: :class:`MSWriterConfig <realtime.receive.modules.consumers.mswriter.MSWriterConsumerConfig>`
.. |plasma-config| replace:: :class:`PlasmaWriterConfig <realtime.receive.modules.consumers.plasma_writer.PlasmaWriterConfig>`
.. |spead2-config| replace:: :class:`Spead2ReceptionConfig <realtime.receive.modules.receivers.spead2_receivers.Spead2ReceptionConfig>`
