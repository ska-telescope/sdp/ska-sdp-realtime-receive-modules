.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.


Realtime-Receive-Modules
========================

The package provides the package and components
to facilitate the reception of data
into the SDP from the LOW and MID CBF.

The receiver code mainly consists
on four different sections:

 * Receivers that listen to data from the network
   and unpack it for further usage.
   Only one receiver, the :class:`realtime.receive.modules.receivers.spead2_receivers.receiver`
   is implemented.
 * A :doc:`data aggregation layer <aggregation>`
   where individual blocks of data received from the network
   are put together before handing them over to the next step.
 * :doc:`Consumers <consumers>` that receive the aggregated blocks of data.
   A few built-in consumers are implemented in this package,
   but users can bring their own.
 * :doc:`Plasma Processors <plasma-processors>`
   (actually not part of this repository,
   but in the :doc:`ska-sdp-realtime-receive-processors:index` project)
   that receive data via the plasma store.
   These only make sense
   when using the :ref:`consumers.plasma_writer` consumer.

.. This diagram can be edited by opening the file directly in https://app.diagrams.net
.. figure:: _static/img/realtime-receive-modules.drawio.svg

Users can also find here the :doc:`api`,
plus a number of historical documents under :doc:`appendix/index`.

Historically this package was part of the :doc:`ska-sdp-cbf-emulator:index` project,
which now contains only code for the CBF emulator (i.e., a payload sender).
The two still share some common code
(e.g., the ICD SPEAD Item definitions),
which can be found in the :doc:`ska-sdp-realtime-receive-core:index` project.

.. toctree::
  :hidden:

  installation
  running
  configuration
  aggregation
  consumers
  plasma-processors
  api
  appendix/index
