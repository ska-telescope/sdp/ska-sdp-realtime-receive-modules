# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

PYTHON_LINE_LENGTH = 99
DOCS_SPHINXOPTS = -W --keep-going
PYTHON_LINT_TARGET = src/ tests/
PYTHON_SWITCHES_FOR_FLAKE8 = --select E --ignore=E501,W503,E731
CASACORE_MEASURES_DIR ?= /usr/share/casacore/data
MEASUREMENT_SETS_FOR_TESTS = sim-vis.ms.tar.gz AA05LOW.ms.tar.gz 1934_SB4094_b0_t0_ch0.ms.tar.gz

python-pre-test:
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"
	./extract_data.sh $(MEASUREMENT_SETS_FOR_TESTS)
	# The images used in CI don't have the netbase package yet
	# (see https://skao.slack.com/archives/CEMF9HXUZ/p1679029600829229)
	# which is needed to run the tests against the pcap files.
	test "${GITLAB_CI}" != "true" || apt install -y netbase

install-doc-requirements-with-poetry:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs

docs-pre-build: install-doc-requirements-with-poetry

python-benchmark: PYTHON_VARS_AFTER_PYTEST = --benchmark-only --benchmark-autosave
python-benchmark: python-test
