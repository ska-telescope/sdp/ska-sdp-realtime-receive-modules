import functools

from astropy import units
from astropy.coordinates.angles import Angle
from overrides import override
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.scan import (
    Beam,
    Channels,
    Field,
    PhaseDirection,
    Polarisations,
    Scan,
    ScanType,
    SpectralWindow,
    StokesType,
)

from .cbf_triggered_scan_provider import CBFTriggeredScanProvider

MAX_CONCURRENT_SCANS = 2
"""
Maximum number of scans ever expected to be received concurrently. 2 is already
a stretch, but is useful for tests.
"""


class HardcodedScanProvider(CBFTriggeredScanProvider):
    """
    Returns always the same ScanType information.
    """

    def __init__(self, scan_type: ScanType):
        super().__init__()
        self._scan_type = scan_type

    @staticmethod
    def from_spectral_window(sw_channels: ChannelRange) -> "HardcodedScanProvider":
        """
        Create a HardcodedScanProvider with a single SpectralWindow with the
        given channel configuration.
        """
        spectral_window = SpectralWindow(
            spectral_window_id="spectral_window_id",
            count=sw_channels.count,
            start=sw_channels.start_id,
            freq_min=1e8,
            freq_max=2e8,
            stride=sw_channels.stride,
        )
        scan_type = ScanType(
            scan_type_id="scan_type_id",
            beams=[
                Beam(
                    beam_id="beam_id",
                    function="function",
                    channels=Channels(
                        channels_id="channels_id",
                        spectral_windows=[spectral_window],
                    ),
                    polarisations=Polarisations(
                        polarisation_id="polarisation_id",
                        correlation_type=[
                            StokesType.XX,
                            StokesType.XY,
                            StokesType.YX,
                            StokesType.YY,
                        ],
                    ),
                    field=Field(
                        field_id="field_id",
                        phase_dir=PhaseDirection(
                            ra=Angle([0], units.rad),
                            dec=Angle([0], units.rad),
                            reference_time="J2000",
                        ),
                    ),
                )
            ],
        )
        return HardcodedScanProvider(scan_type)

    @override
    async def first_scan_data_received(self, scan_id: int) -> None:
        await self._set_and_start_current_scan(Scan(scan_id, self._scan_type))

    @override
    @functools.lru_cache(MAX_CONCURRENT_SCANS)
    def get(self, scan_id: int) -> Scan | None:
        return Scan(scan_id, self._scan_type)
