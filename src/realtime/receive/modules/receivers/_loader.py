import importlib
import logging

from realtime.receive.core.common import from_dict

from ._config import Config

logger = logging.getLogger(__name__)


def _build_if_not_concrete_config_instance(config: Config, config_class: type):
    assert issubclass(config_class, Config)
    return config if isinstance(config, config_class) else config_class()


def _get_receiver_class(config: Config):
    method = config.method.lower()
    m = importlib.import_module(".." + method, package=__name__)
    return getattr(m, "receiver")


def create_config(**kwargs):
    """Generate a config object from the given kwargs"""
    generic_config = from_dict(Config, kwargs)
    return from_dict(_get_receiver_class(generic_config).config_class, kwargs)


def create(config, *args, **kwargs):
    """Create a receiver for the given config"""
    receiver_class = _get_receiver_class(config)
    config = _build_if_not_concrete_config_instance(config, receiver_class.config_class)
    logger.info("Creating receiver with config: %r", config)
    return receiver_class(config, *args, **kwargs)
