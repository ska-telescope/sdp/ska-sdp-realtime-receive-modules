import asyncio
import logging
import time
from collections.abc import Awaitable, Callable
from contextlib import AbstractAsyncContextManager

logger = logging.getLogger(__name__)


class PeriodicTask(AbstractAsyncContextManager):
    """
    A class encapsulating the periodic execution of a coroutine. The period is
    respected as closely as possible, but if the coroutine takes longer than the
    period it is not cancelled, and it will immediately rerun. The coroutine is
    *not* executed on `astop()`.
    """

    def __init__(self, period: float, callback: Callable[[], Awaitable[None]]):
        self._period = period
        self._callback = callback
        self._event = asyncio.Event()
        self._task: asyncio.Task | None = None

    async def astart(self):
        """Start the periodic background task."""
        self._task = asyncio.create_task(self._periodic_execution())

    async def astop(self):
        """Stop the periodic background task issued by this object."""
        self._event.set()
        await self._task

    async def _periodic_execution(self):
        callback_execution_duration = 0.0
        while True:
            next_period = max(self._period - callback_execution_duration, 0)
            try:
                await asyncio.wait_for(self._event.wait(), next_period)
            except asyncio.TimeoutError:
                callback_execution_duration = await self._run_callback()
            else:
                break

    async def _run_callback(self) -> float:
        start = time.monotonic()
        try:
            await self._callback()
        except Exception:  # pylint: disable=broad-except
            logger.exception("Unexpected error occurred")
        finally:
            end = time.monotonic()
        return end - start

    async def __aenter__(self):
        await self.astart()
        return self

    async def __aexit__(self, *_args, **_kwargs):
        await self.astop()
