import numpy as np
from realtime.receive.core import time_utils
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.scan import Scan, SpectralWindow
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import Visibility

from realtime.receive.modules.payload_data import PayloadData
from realtime.receive.modules.tm.base_tm import TelescopeManager


def _icd_to_visibility_dim_order(array):
    """
    Swaps baseline and channel dimensions for a time,channel,baseline,pol array
    """
    return np.moveaxis(array, [1, 2], [2, 1])


def _resolve_freq_vars(
    scan: Scan, first_channel_id: int, num_channels: int
) -> tuple[ChannelRange, np.ndarray, np.ndarray]:
    """
    Given a Scan, a channel ID and a number of channels, determines the
    Spectral Window corresponding to that channel ID, and the full set of
    channel IDs, frequencies and channel bandwidths for the total number of
    channels.
    """

    spectral_window, channel_ids = _find_spectral_window_and_channels(
        scan, first_channel_id, num_channels
    )
    frequencies = spectral_window.frequencies_for_range(channel_ids)
    bandwidths = np.repeat(spectral_window.channel_bandwidth, num_channels)
    return channel_ids, frequencies, bandwidths


def _find_spectral_window_and_channels(
    scan: Scan, channel_id: int, channel_count: int
) -> tuple[SpectralWindow, ChannelRange]:
    """
    Find the SpectralWindow and Channel ID range corresponding to the given
    channel ID and count
    """

    # TODO(yan-1432) In theory, if this is Low data, we should know
    # which beam this visibility data is for but we currently don't
    # include it in the visibility object.
    assert scan
    for b in scan.scan_type.beams:
        for sw in b.channels.spectral_windows:
            # Assuming that channels are sent in continuous ranges of the
            # spectral window i.e. The stride of sent visibilities matches
            # the stride of the spectral window.
            # We have to do this as no stride information is sent over
            # spead, just the first channel id and the number of channels
            if channels := sw.try_as_channel_range(
                start_id=channel_id,
                count=channel_count,
            ):
                return sw, channels

    raise ValueError(
        "Unable to find spectral window which matches channel information"
        f" (looking for channel id={channel_id}, count={channel_count} in"
        f" {scan})"
    )


class VisibilityBuilder:
    """
    A class that builds an SDP Visibility object from many ICD payloads.

    When this class is created, it internally creates a series of numpy arrays
    of a fixed size holding the data that will eventually make up the SDP
    Visibility. These arrays are populated by copying data from individual ICD
    payloads into the correct slices, depending on the ICD payload time/channel
    information, and this VisibilityBuilder's time/channel span. Finally, users
    can obtain an SDP Visibility object made up of the collected data. After
    the Visibility object is required, no more data can be appended to this
    builder, nor a new Visibility object can be requested.

    The visibility data starts fully flagged, and slices are unflagged as data
    is appended.
    """

    def __init__(
        self,
        vis_shape: tuple[int, int, int, int],
        scan: Scan,
        tm: TelescopeManager,
        first_time_step: int,
        first_channel_id: int,
    ):
        self._vis_shape = vis_shape
        self._scan = scan
        self._tm = tm
        self._visibility_built = False

        self._gauge = np.zeros(shape=(self.num_time_steps, self.num_channels), dtype=np.bool_)
        self._cells_to_fill = self.num_time_steps * self.num_channels
        self._cells_filled = 0

        # The core of the data, initialised here and overwritten in add_payload
        # (except for UVWs, which are calculated later on-demand)
        # The full block is flagged by default, we unflag in add_payload
        stacked_correlated_data_shape = (
            self.num_time_steps,
            self.num_channels,
            self.num_baselines,
        )
        stacked_icd_vis_shape = stacked_correlated_data_shape + (self.num_pol,)
        uvw_shape = (self.num_time_steps, self.num_baselines, 3)
        self._uvw = np.zeros(shape=uvw_shape, dtype=np.float64)
        self._vis = np.zeros(shape=stacked_icd_vis_shape, dtype=np.complex64)
        self._corr_data_fraction = np.zeros(shape=stacked_correlated_data_shape, dtype=np.uint8)

        # Channel IDs, frequencies and bandwidths
        channel_ids, frequency, channel_bandwidth = _resolve_freq_vars(
            scan, first_channel_id, self.num_channels
        )
        self._channel_ids = channel_ids
        self._frequency = frequency
        self._channel_bandwidth = channel_bandwidth

        # Time steps are sequential
        self._payload_seq_numbers = np.arange(
            first_time_step,
            first_time_step + self.num_time_steps,
            dtype=np.int64,
        )

        # Times are dummy zero values, they are updated with the payloads'
        # timestamps, then converted to MJD when constructing the Visibility
        self._times = np.zeros(shape=(self.num_time_steps,), dtype=np.float64)

    @property
    def is_full(self) -> bool:
        """
        Whether the Visibility built by this builder is full of data, and thus
        ready to be taken
        """
        return self._cells_filled == self._cells_to_fill

    @property
    def flagged_fraction(self) -> bool:
        """The fraction of visibility data that is flagged as missing."""
        return (self._cells_to_fill - self._cells_filled) / self._cells_to_fill

    @property
    def is_empty(self) -> bool:
        """
        Whether the Visibility built by this builder is completely empty.
        """
        return self._cells_filled == 0

    @property
    def num_time_steps(self) -> int:
        """
        The number of time steps in the resulting Visibility.
        """
        return self._vis_shape[0]

    @property
    def num_baselines(self) -> int:
        """
        The number of baselines in the resulting Visibility.
        """
        return self._vis_shape[1]

    @property
    def num_channels(self) -> int:
        """
        The number of frequency channels in the resulting Visibility.
        """
        return self._vis_shape[2]

    @property
    def num_pol(self) -> int:
        """
        The number of polarisations in the resulting Visibility.
        """
        return self._vis_shape[3]

    def _check_visibility_not_built(self):
        if self._visibility_built:
            raise RuntimeError("Cannot use VisibilityBuilder after calling get_visibility()")

    def add_payload(self, payload: PayloadData):
        """
        Adds a payload into this builder so its data will be contained in the
        resulting Visibility object.
        """
        self._check_visibility_not_built()

        if not (
            self._payload_seq_numbers[0]
            <= payload.sequence_number
            <= self._payload_seq_numbers[-1]
        ):
            raise ValueError(
                f"sequence number {payload.sequence_number} out of range for this VisibilityBuilder"
            )
        if not self._channel_ids.contains_channel(payload.first_channel_id):
            raise ValueError(
                f"channel_id {payload.first_channel_id} out of range for this VisibilityBuilder"
            )
        time_idx = payload.sequence_number - self._payload_seq_numbers[0]
        first_channel_idx = (
            payload.first_channel_id - self._channel_ids.start_id
        ) // self._channel_ids.stride

        expected_vis_shape = (
            self.num_baselines,
            self.num_pol,
        )
        assert (
            payload.visibilities.shape[-2:] == expected_vis_shape
        ), f"Visibility shape {payload.visibilities.shape} different from expected {expected_vis_shape}"

        # fast path for channel_count == 1, which uses direct indexing for improved performance
        if payload.channel_count == 1:
            channel_idx = first_channel_idx
            if self._gauge[time_idx, channel_idx]:
                raise ValueError("Value for time/channel already found")
            self._times[time_idx] = payload.timestamp
            self._vis[time_idx, channel_idx] = payload.visibilities
            self._corr_data_fraction[time_idx, channel_idx] = payload.correlated_data_fraction
            self._gauge[time_idx, channel_idx] = True
            self._cells_filled += payload.channel_count
        else:
            channel_idx_slice = slice(first_channel_idx, first_channel_idx + payload.channel_count)
            if np.any(self._gauge[time_idx, channel_idx_slice]):
                raise ValueError("Value for time/channel already found")
            self._times[time_idx] = payload.timestamp
            self._vis[time_idx, channel_idx_slice] = payload.visibilities
            self._corr_data_fraction[time_idx, channel_idx_slice] = (
                payload.correlated_data_fraction
            )
            self._gauge[time_idx, channel_idx_slice] = True
            self._cells_filled += payload.channel_count

    def get_visibility(self) -> Visibility:
        """
        Create and return a Visibility object from the data that has been
        aggregated into this builder.

        After calling this method, no more data should be appended to this
        builder, nor a Visibility should be requested again.
        """
        self._check_visibility_not_built()

        # TODO (rtobar): clarify where we'll get this information from
        integration_time = np.ones(self.num_time_steps)

        # CBF encodes data fractions as a uint8 FD = sqrt(R) * 255, where R
        # is the actual ratio of samples included in the integration.
        # We can interpret these as weight without further value manipulation
        # since weights are normalised anyway.
        # CBF sends a correlated data fraction per freq/baseline, so we have
        # to expand them (and change their type) to have them per visibility,
        # which is what's expected down the pipeline
        corr_data_frac = self._corr_data_fraction.astype(np.float32)
        weights = _icd_to_visibility_dim_order(
            np.broadcast_to(
                corr_data_frac[..., np.newaxis],
                corr_data_frac.shape + (self.num_pol,),
            )
        )

        # Our gauge (we keep it only for time/channel) is our flags, but with
        # the inverse value
        raw_flags = np.logical_not(self._gauge)
        flags = _icd_to_visibility_dim_order(
            np.broadcast_to(
                raw_flags[..., np.newaxis, np.newaxis],
                raw_flags.shape + (self.num_baselines, self.num_pol),
            )
        )
        # TODO (s.ord) I note here that the constructor assumes a polarisation order
        # this may not be the case.
        # YAN-1803: The polarisation frame is hardcoded to "linear" for now.

        visibility = Visibility.constructor(
            vis=_icd_to_visibility_dim_order(self._vis),
            configuration=self._tm.as_configuration,
            flags=flags,
            baselines=self._tm.baselines_as_visibility_indices,
            uvw=self._uvw,
            channel_bandwidth=self._channel_bandwidth,
            frequency=self._frequency,
            time=time_utils.unix_to_mjd(self._times),
            polarisation_frame=PolarisationFrame("linear"),
            integration_time=integration_time,
            weight=weights,
            meta={
                "scan": self._scan,
                "payload_seq_numbers": self._payload_seq_numbers,
                "channel_ids": self._channel_ids.as_np_range(np.int64),
            },
            low_precision="float32",
        )
        self._visibility_built = True
        return visibility


def create_visibility(
    payloads: list[PayloadData],
    tm: TelescopeManager,
):
    """
    Create an SDP Visibility class from the given sequence of payload data. The
    payloads must belong to the same channel and scan, and must be sequential
    in time.

    :param payloads: A list of PayloadData objects.
    :param tm: The Telescope Manager
    """

    # All payloads belong to the same channel/stream and scan
    assert len({payload.scan.scan_number for payload in payloads}) == 1
    assert len({payload.first_channel_id for payload in payloads}) == 1
    if __debug__:
        sequence_numbers = [payload.sequence_number for payload in payloads]
        diff = np.unique(np.diff(sequence_numbers))
        all_sequential = len(diff) == 0 or (len(diff) == 1 and diff[0] == 1)
        if not all_sequential:
            raise AssertionError(f"payloads are not sequential in time: {sequence_numbers}")

    num_time_steps = len(payloads)
    num_channels, num_baselines, num_pols = payloads[0].visibilities.shape
    vis_shape = (num_time_steps, num_baselines, num_channels, num_pols)
    vis_builder = VisibilityBuilder(
        vis_shape,
        payloads[0].scan,
        tm,
        payloads[0].sequence_number,
        payloads[0].first_channel_id,
    )
    for payload in payloads:
        vis_builder.add_payload(payload)
    assert vis_builder.is_full
    return vis_builder.get_visibility()
