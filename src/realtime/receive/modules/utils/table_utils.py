"""Utility methods to construct table-like objects from simple quantities"""

import numpy as np
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.visibility import Visibility


def get_spectral_window_and_channels(num_channels: int, freq_start_hz: float, freq_inc_hz: float):
    """
    Return a simple spectral window table and a simple channels dict for the
    given frequency specification, which corresponds to a single frequency band.
    """

    # chan_freqs is at the center of each channel range
    chan_freqs = np.linspace(
        freq_start_hz,
        freq_start_hz + num_channels * freq_inc_hz,
        num=num_channels,
    )
    chan_widths = np.repeat(freq_inc_hz, num_channels)
    chan_start = chan_freqs - chan_widths / 2
    chan_end = chan_freqs + chan_widths / 2
    total_bw = np.max(chan_end) - np.min(chan_start)

    spectral_window = {
        "name": ["SpectralWindow0"],
        "ref_frequency": [freq_start_hz],
        "total_bandwidth": [total_bw],
    }
    channels = {
        "frequency": chan_freqs,
        "width": chan_widths,
        "window": np.repeat(0, num_channels),
    }
    return spectral_window, channels


def get_antennas(configuration: Configuration):
    """Return a simple antenna dict with the given configuration."""
    return {
        "name": configuration.names.data,
        "mount": configuration.mount.data,
        # "xyz" is written into a pyarrow.list_(size=3), so needs to be 1D
        "xyz": list(configuration.xyz.data),
        "dish_diameter": configuration.diameter.data,
    }


def get_baselines(visibility: Visibility):
    """
    Return a simple baseline index dict for the given Baselines object.
    """
    baselines = visibility.coords["baselines"]
    return {"antenna1": baselines.antenna1, "antenna2": baselines.antenna2}


def get_field(phase_centre_radec_rad):
    """Return a simple field table with the given phase centre."""
    return {"phase_dir": [{"ra": phase_centre_radec_rad[0], "dec": phase_centre_radec_rad[1]}]}


def get_polarization(num_pols):
    """
    Return a simple polarisation table with the given number of polarisations
    """
    return {"name": [f"POL{i}" for i in range(num_pols)]}
