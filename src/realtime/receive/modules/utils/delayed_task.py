"""A class that delays the execution of an awaitable"""

import asyncio
from typing import Awaitable, Callable


class DelayedTask:
    """
    A task that delays the execution of an awaitable for a given amount of time. If the callback
    hasn't been executed yet, any remaining delay can be cancelled.
    """

    def __init__(self, delay: float, callback: Callable[[], Awaitable[None]]):
        self._event = asyncio.Event()
        self._task = asyncio.create_task(self._delay_execution(delay, callback))

    def cancel_delay(self):
        """Cancel any remaining delay, allowing the callback to be executed soon"""
        self._event.set()

    async def _delay_execution(self, delay: float, callback: Callable[[], Awaitable[None]]):
        try:
            await asyncio.wait_for(self._event.wait(), delay)
        except asyncio.TimeoutError:
            pass
        await callback()

    def __await__(self):
        return self._task.__await__()
