from realtime.receive.core import Antenna, Baselines

from realtime.receive.modules.tm.base_tm import TelescopeManager


class HardcodedTelescopeManager(TelescopeManager):
    """Telescope Manager created with a given number of dummy antennas"""

    def __init__(
        self,
        num_stations: int,
        auto_corr: bool = True,
        lower_triangular: bool = True,
    ):
        antennas = [
            Antenna(
                interface="",
                station_label=f"S{n}",
                station_id=n,
                diameter=12,
                location={"geocentric": {"x": 0, "y": 0, "z": 0}},
                fixed_delays={},
                niao=0,
            )
            for n in range(num_stations)
        ]
        baselines = Baselines.generate(
            num_stations, autocorr=auto_corr, lower_triangular=lower_triangular
        )
        super().__init__(antennas, baselines)
