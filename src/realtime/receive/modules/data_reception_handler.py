"""Data reception interface definition"""

import abc


class DataReceptionHandler(abc.ABC):
    """
    Interface implemented by objects that want to be notified about data reception
    events.
    """

    @abc.abstractmethod
    async def first_scan_data_received(self, scan_id: int) -> None:
        """The first piece of data has been received from CBF"""

    @abc.abstractmethod
    async def last_scan_data_received(self) -> None:
        """
        Called to notify this object that no more visibility data will be received for the current
        scan from CBF.
        """
