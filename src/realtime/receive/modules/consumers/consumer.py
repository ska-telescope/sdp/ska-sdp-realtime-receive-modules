from abc import ABC, abstractmethod

import numpy as np
from astropy.time import Time
from realtime.receive.core.uvw_engine import UVWEngine
from ska_sdp_datamodels.visibility import Visibility

from realtime.receive.modules.scan_lifecycle_handler import ScanLifecycleHandler
from realtime.receive.modules.tm import TelescopeManager

from ._config import Config

_SECS_PER_DAY = 86400


class Consumer(ScanLifecycleHandler, ABC):
    """
    An interface class providing an interface for
    consuming received Visibility objects.

    Subclasses should implement the required abstract methods. In addition, if
    they provide a custom ``__init__`` function, it should match the signature
    of this base class' ``__init__`` function, forwarding all arguments. This
    is required given the dynamic loading nature of consumers, which are all
    instantiated in the same manner.
    """

    config_class = Config

    def __init__(
        self,
        config: Config,
        tm: TelescopeManager,
        uvw_engine: UVWEngine,
    ):
        """
        Generic constructor used to create sdp_receive consumers.
        Args:
            config (ConfigParser): config parser
            tm (TelescopeManager): a telescope model
            uvw_engine (UVWEngine): uvw calculation engine
        """
        self._config = config
        self._tm = tm
        self._uvw_engine = uvw_engine

    @property
    def tm(self) -> TelescopeManager:
        """Returns the TelescopeManager used by this Consumer"""
        return self._tm

    @property
    def config(self) -> Config:
        """Returns the config for this Consumer"""
        return self._config

    async def start_scan(self, scan_id: int) -> None:
        pass

    async def end_scan(self, scan_id: int) -> None:
        pass

    def _get_uvw(self, visibility: Visibility, scan):
        """
        Returns the UVWs for the given visibility and scan, useful for derived
        classes.
        """
        # TODO: This assumes we're interesting in beam 0 only
        # TODO: UVWEngine classes can't deal with multiple times, see YAN-1429
        return np.stack(
            [
                self._uvw_engine.get_uvw(
                    vis_time,
                    phase_direction=scan.scan_type.beams[0].field.phase_dir,
                )
                for vis_time in Time(visibility.time / _SECS_PER_DAY, format="mjd")
            ]
        )

    @abstractmethod
    async def consume(self, visibility: Visibility):
        """
        Asyncronously consumes/processes an incoming Visibility.

        :param visibility: visibilities dataset
        """
        raise NotImplementedError

    async def astop(self):
        """
        Called when receiver stream ends.

        Users should implement their own astop method. To maintain backwards
        compatibility this default implementation invokes the old, blocking
        stop() method.
        """

    async def __aenter__(self):
        return self

    async def __aexit__(self, _err, _typ, _traceback):
        await self.astop()
