import warnings

# These are import for backwards-comatibility
# pylint: disable-next=unused-import
from realtime.receive.modules.scan_providers import (
    AssignResourcesScanProvider,
    HardcodedScanProvider,
    MSScanProvider,
    ScanProvider,
    SdpConfigScanProvider,
)

warnings.warn(
    f"The {__name__} module is now deprecated, its contents have been moved to the "
    f"{'.'.join(__name__.split('.')[:-1])}.scan_providers package, import and use that instead",
    DeprecationWarning,
    stacklevel=2,
)
