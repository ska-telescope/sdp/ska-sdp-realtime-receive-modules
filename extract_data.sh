#!/bin/sh

cd data
for file in "$@"; do
    extracted_name=${file%%.tar.gz}
    test ! -e "$extracted_name" || { echo "Skipping $file since $extracted_name exists" && continue; }
    echo "Extracting $file"
    tar xf $file
done
